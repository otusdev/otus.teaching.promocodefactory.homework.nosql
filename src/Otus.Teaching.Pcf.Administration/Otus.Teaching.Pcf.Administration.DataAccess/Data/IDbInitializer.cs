﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}